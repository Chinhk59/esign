package com.lendbiz.esign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LbEsignApplication {

	public static void main(String[] args) {
		SpringApplication.run(LbEsignApplication.class, args);
	}

}
