/***************************************************************************
 * Copyright 2018 by VIETIS - All rights reserved.                *    
 **************************************************************************/
package com.lendbiz.esign.dto.response;

import java.io.Serializable;

import org.springframework.stereotype.Controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Controller
@AllArgsConstructor
@NoArgsConstructor
public class LbResponse<T> implements Serializable{
  private static final long serialVersionUID = 1L;
  
  protected String status;
  protected String message;
  
  protected T data;
  
}
