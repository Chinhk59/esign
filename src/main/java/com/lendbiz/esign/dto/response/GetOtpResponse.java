package com.lendbiz.esign.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GetOtpResponse {
	private Long cloudCertificateOwnerID;
	private Long cloudCertificateID;
	private Long responseCode;
	private String responseMessage;
	private String billCode;
	private Long timestamp;
	private Long logInstance;
	private Long remainingCounter;
	private Long authorizeMethod;
	private Long certificateStateID;
	private Long credentialExpireIn;
}
