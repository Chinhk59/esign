package com.lendbiz.esign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lendbiz.esign.constants.JsonMapper;
import com.lendbiz.esign.dto.request.AuthCounterSignRequest;
import com.lendbiz.esign.dto.request.PrepareCertificateRequest;
import com.lendbiz.esign.dto.request.PrepareMultipleFileRequest;
import com.lendbiz.esign.dto.request.ReGenerateAuthCodeRequest;
import com.lendbiz.esign.service.EsignService;

@RestController
@RequestMapping("/esign/v1.0")
public class EsignController extends AbstractController<EsignService> {
	
	@Autowired
	private EsignService service;
	
	@PostMapping("/register")
	public ResponseEntity<?> register(@RequestBody PrepareCertificateRequest input) throws Exception {
		logger.info("[ESIGN REGISTER] request param:" + JsonMapper.writeValueAsString(input));

		return response(toResult(service.prepareCertificateForSignCloud(input)));
	}
	
	@PostMapping("/upload-contract")
	public ResponseEntity<?> register(@RequestBody PrepareMultipleFileRequest input) throws Exception {
		logger.info("[ESIGN UPLOAD CONTRACT] request param:" + JsonMapper.writeValueAsString(input));

		return response(toResult(service.prepareMultipleFilesForSignCloud(input)));
	}

	@PostMapping("/verify-otp")
	public ResponseEntity<?> register(@RequestBody AuthCounterSignRequest input) throws Exception {
		logger.info("[ESIGN VERIFY OTP] request param:" + JsonMapper.writeValueAsString(input));

		return response(toResult(service.authorizeCounterSigningForSignCloud(input)));
	}

	@PostMapping("/regenerate-otp")
	public ResponseEntity<?> register(@RequestBody ReGenerateAuthCodeRequest input) throws Exception {
		logger.info("[ESIGN REGENERATE OTP] request param:" + JsonMapper.writeValueAsString(input));

		return response(toResult(service.regenerateAuthorizationCodeForSignCloud(input)));
	}
}
