package com.lendbiz.esign.constants;

public class Constants {
	
	final public static int AUTHORISATION_METHOD_SMS = 1;
    final public static int AUTHORISATION_METHOD_EMAIL = 2;
    final public static int AUTHORISATION_METHOD_MOBILE = 3;
    final public static int AUTHORISATION_METHOD_PASSCODE = 4;
    final public static int AUTHORISATION_METHOD_UAF = 5;
    
    final public static int ASYNCHRONOUS_CLIENTSERVER = 1;
    final public static int ASYNCHRONOUS_SERVERSERVER = 2;
    final public static int SYNCHRONOUS = 3;
    
    final public static String MIMETYPE_PDF = "application/pdf";
    final public static String MIMETYPE_XML = "application/xml";
    final public static String MIMETYPE_XHTML_XML = "application/xhtml+xml";
    
    final public static String MIMETYPE_BINARY_WORD = "application/msword";
    final public static String MIMETYPE_OPENXML_WORD = "application/ vnd.openxmlformats-officedocument.wordprocessingml.document";
    final public static String MIMETYPE_BINARY_POWERPOINT = "application/vnd.ms-powerpoint";
    final public static String MIMETYPE_OPENXML_POWERPOINT = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    final public static String MIMETYPE_BINARY_EXCEL = "application/vnd.ms-excel";
    final public static String MIMETYPE_OPENXML_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    final public static String MIMETYPE_MSVISIO = "application/vnd.visio";
    
    final public static String MIMETYPE_SHA1 = "application/sha1-binary";
    final public static String MIMETYPE_SHA256 = "application/sha256-binary";
    final public static String MIMETYPE_SHA384 = "application/sha384-binary";
    final public static String MIMETYPE_SHA512 = "application/sha512-binary";

    final public static String URL = "https://rssp.mobile-id.vn/eSignCloud/restapi/";

    final public static String relyingParty = "LENDBIZ";
    final public static String relyingPartyUser = "lenbizdemo";
    final public static String relyingPartyPassword = "12345678";
    final public static String relyingPartySignature = "WEEn3tAZwyCZs2RKGYYcYGRgCQDuuV25S/Ec2MY/FjyMs2w0NaBoXXo9dj+F5mtoYe1NGVAH0J8yTKkLNEglFQURidbFkItLHaS1+kj4Ve9ZPKNjmwN5+L3aycGyb7ed4ReiX6TNR/c23K6CZZ7Z0tuDmo9mPkf169SV3dKdIynkr3wS/9T6mLjWLydFmA5jh6ZUmE90/4A3Or/AY846YmzScnPD/MQuBXL29T5N5T3w19ASm0+yMYhlWW5WI0sYsZPtdoJA8UPG0SnbLKtyCYM2qScnpi9N0mY3fH6Q+cAgdTyYPmbmGKmXDeRQUkkaq8xLTX53oL0732p+oyMwMg==";
    final public static String relyingPartyKeyStore = "file\\LENDBIZ.p12";
    final public static String relyingPartyKeyStorePassword = "12345678";

    final public static String CERTIFICATEPROFILE = "PERS.1D";

    final public static String FUNCTION_GETCERTIFICATEDETAILFORSIGNCLOUD = "getCertificateDetailForSignCloud";
    final public static String FUNCTION_PREPAREHASHSIGNINGFORSIGNCLOUD = "prepareHashSigningForSignCloud";
    final public static String FUNCTION_PREPARECERTIFICATEFORSIGNCLOUD = "prepareCertificateForSignCloud";
    final public static String FUNCTION_REGENERATEAUTHORIZATIONCODEFORSIGNCLOUD = "regenerateAuthorizationCodeForSignCloud";
    final public static String FUNCTION_AUTHORIZEHASHSIGNINGFORSIGNCLOUD = "authorizeHashSigningForSignCloud";
    final public static String FUNCTION_PREPAREFILEFORSIGNCLOUD = "prepareFileForSignCloud";
    final public static String FUNCTION_AUTHORIZECOUNTERSIGNINGFORSIGNCLOUD = "authorizeCounterSigningForSignCloud";
}
