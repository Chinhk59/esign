package com.lendbiz.esign.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lendbiz.esign.constants.Constants;
import com.lendbiz.esign.constants.JsonMapper;
import com.lendbiz.esign.dto.request.AuthCounterSignRequest;
import com.lendbiz.esign.dto.request.PrepareCertificateRequest;
import com.lendbiz.esign.dto.request.PrepareMultipleFileRequest;
import com.lendbiz.esign.dto.request.ReGenerateAuthCodeRequest;
import com.lendbiz.esign.model.AgreementDetails;
import com.lendbiz.esign.model.CredentialData;
import com.lendbiz.esign.model.MultipleSignedFileData;
import com.lendbiz.esign.model.MultipleSigningFileData;
import com.lendbiz.esign.model.SignCloudMetaData;
import com.lendbiz.esign.model.SignCloudReq;
import com.lendbiz.esign.model.SignCloudResp;
import com.lendbiz.esign.model.exception.BusinessException;
import com.lendbiz.esign.service.BaseService;
import com.lendbiz.esign.service.EsignService;
import com.lendbiz.esign.utils.Utils;

@Service("EsignService")
public class EsignServiceImpl extends BaseService implements EsignService {

	final public static String FILE_DIRECTORY = "file\\";
	private static final String USER_AGENT = "Mozilla/5.0";
	
	public EsignServiceImpl(Environment env) {
		super(env);
	}

	public String getCertificateDetailForSignCloud(String agreementUUID) throws Exception {

		SignCloudReq signCloudReq = new SignCloudReq();

		signCloudReq.setRelyingParty(Constants.relyingParty);
		signCloudReq.setAgreementUUID(agreementUUID);

		CredentialData credentialData = createCredentialData();
		signCloudReq.setCredentialData(credentialData);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = sendPost(Constants.FUNCTION_GETCERTIFICATEDETAILFORSIGNCLOUD,
				objectMapper.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

		logger.info("[getCertificateDetailForSignCloud] signCloudResp : {}",
				JsonMapper.writeValueAsString(signCloudResp));
		return jsonResponse;
	}

	public String prepareHashSigningForSignCloud(String agreementUUID, int authorizeMethod, String authorizeCode,
			String notificationTemplate, String notificationSubject, String algorithm, List<String> hashes)
			throws Exception {

		String timestamp = String.valueOf(System.currentTimeMillis());

		String data2sign = Constants.relyingPartyUser + Constants.relyingPartyPassword + Constants.relyingPartySignature
				+ timestamp;
		String pkcs1Signature = Utils.getPKCS1Signature(data2sign, Constants.relyingPartyKeyStore,
				Constants.relyingPartyKeyStorePassword);

		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(Constants.relyingParty);
		signCloudReq.setAgreementUUID(agreementUUID);

		signCloudReq.setAuthorizeMethod(authorizeMethod);
		signCloudReq.setMessagingMode(Constants.SYNCHRONOUS);
		signCloudReq.setAuthorizeCode(authorizeCode);

		signCloudReq.setNotificationTemplate(notificationTemplate);
		signCloudReq.setNotificationSubject(notificationSubject);

		List<MultipleSigningFileData> listOfMultipleSigningFileData = new ArrayList<>();
		for (int i = 0; i < hashes.size(); i++) {
			MultipleSigningFileData multipleSigningFileData = new MultipleSigningFileData();
			multipleSigningFileData.setHash(hashes.get(i));
			multipleSigningFileData.setMimeType(Constants.MIMETYPE_SHA256);
			multipleSigningFileData.setSigningFileName("Hash_Name_" + System.currentTimeMillis());
			listOfMultipleSigningFileData.add(multipleSigningFileData);
		}
		signCloudReq.setMultipleSigningFileData(listOfMultipleSigningFileData);

		CredentialData credentialData = new CredentialData();
		credentialData.setUsername(Constants.relyingPartyUser);
		credentialData.setPassword(Constants.relyingPartyPassword);
		credentialData.setTimestamp(timestamp);
		credentialData.setSignature(Constants.relyingPartySignature);
		credentialData.setPkcs1Signature(pkcs1Signature);
		signCloudReq.setCredentialData(credentialData);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = sendPost(Constants.FUNCTION_PREPAREHASHSIGNINGFORSIGNCLOUD,
				objectMapper.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

		logger.info("[prepareHashSigningForSignCloud] signCloudResp : {}",
				JsonMapper.writeValueAsString(signCloudResp));
		return jsonResponse;
	}

    @Override
	public String prepareCertificateForSignCloud(PrepareCertificateRequest request) throws Exception {
    	
		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(Constants.relyingParty);
		signCloudReq.setAgreementUUID(request.getAgreementUUID());
		signCloudReq.setEmail(request.getAuthorizationEmail());
		signCloudReq.setMobileNo(request.getAuthorizationMobileNo());

		signCloudReq.setCertificateProfile(Constants.CERTIFICATEPROFILE);
		
		AgreementDetails agreementDetails = new AgreementDetails();
		agreementDetails.setPersonalName(request.getPersonalName());
		agreementDetails.setPersonalID(request.getPersonalID());
		agreementDetails.setCitizenID(request.getCitizenID()); // at least personalID or citizenID required
		agreementDetails.setLocation(request.getLocation());
		agreementDetails.setStateOrProvince(request.getStateProvince());
		agreementDetails.setCountry(request.getCountry());
		
		// CMND
		agreementDetails.setPhotoFrontSideIDCard(request.getFrontSideOfIDDocument());
		agreementDetails.setPhotoBackSideIDCard(request.getBackSideOfIDDocument());

		signCloudReq.setAgreementDetails(agreementDetails);
		CredentialData credentialData = createCredentialData();
		signCloudReq.setCredentialData(credentialData);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = sendPost(Constants.FUNCTION_PREPARECERTIFICATEFORSIGNCLOUD,
				objectMapper.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

		logger.info("[prepareCertificateForSignCloud] signCloudResp: {}", JsonMapper.writeValueAsString(signCloudResp));
		return signCloudResp.getResponseMessage().toString();
	}

	private CredentialData createCredentialData() throws Exception {
		String timestamp = String.valueOf(System.currentTimeMillis());

		String data2sign = Constants.relyingPartyUser + Constants.relyingPartyPassword + Constants.relyingPartySignature
				+ timestamp;
		String pkcs1Signature = Utils.getPKCS1Signature(data2sign, Constants.relyingPartyKeyStore,
				Constants.relyingPartyKeyStorePassword);

		CredentialData credentialData = new CredentialData();
		credentialData.setUsername(Constants.relyingPartyUser);
		credentialData.setPassword(Constants.relyingPartyPassword);
		credentialData.setTimestamp(timestamp);
		credentialData.setSignature(Constants.relyingPartySignature);

		credentialData.setPkcs1Signature(pkcs1Signature);
		return credentialData;
	}

	public String authorizeHashSigningForSignCloud(String agreementUUID, String authorizeCode, String billCode)
			throws Exception {

		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(Constants.relyingParty);
		signCloudReq.setAgreementUUID(agreementUUID);
		signCloudReq.setMessagingMode(Constants.SYNCHRONOUS);
		signCloudReq.setBillCode(billCode);
		signCloudReq.setAuthorizeCode(authorizeCode);

		CredentialData credentialData = createCredentialData();
		signCloudReq.setCredentialData(credentialData);
		signCloudReq.setCertificateRequired(true);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = sendPost(Constants.FUNCTION_AUTHORIZEHASHSIGNINGFORSIGNCLOUD,
				objectMapper.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

		logger.info("[authorizeHashSigningForSignCloud] signCloudResp response: {}", JsonMapper.writeValueAsString(signCloudResp));
		return jsonResponse;
	}

	private String sendPost(String function, String payload) throws Exception {
		logger.info("[sendPost] start send post with payload : {}", payload);

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
		});
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		String endpointUrl = Constants.URL + function;
		URL obj = new URL(endpointUrl);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.write(payload.getBytes("UTF-8"));
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		if (responseCode != 200) {
			throw new BusinessException("08", "Error while calling eSignCloud server");
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		logger.info("[sendPost] Response: {}" + response);
		return response.toString();
	}

    @Override
    public String regenerateAuthorizationCodeForSignCloud(ReGenerateAuthCodeRequest request) throws Exception {

        SignCloudReq signCloudReq = new SignCloudReq();
        signCloudReq.setRelyingParty(Constants.relyingParty);
        signCloudReq.setAgreementUUID(request.getAgreementUUID());
        signCloudReq.setAuthorizeMethod(request.getAuthorizeMethod());

        signCloudReq.setNotificationTemplate(request.getNotificationTemplate());
        signCloudReq.setNotificationSubject(request.getNotificationSubject());

        CredentialData credentialData = createCredentialData();
        signCloudReq.setCredentialData(credentialData);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = sendPost(Constants.FUNCTION_REGENERATEAUTHORIZATIONCODEFORSIGNCLOUD, objectMapper.writeValueAsString(signCloudReq));
        SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

        logger.info("[regenerateAuthorizationCodeForSignCloud] signCloudResp : {}", JsonMapper.writeValueAsString(signCloudResp));
        
        return "Thanh cong";
    }

    public String prepareFileForSignCloud(
            String agreementUUID,
            int authorizeMethod,
            String authorizeCode,
            String notificationTemplate,
            String notificationSubject,
            byte[] sigingFileData,
            String signingFileName,
            String mimeType,
            SignCloudMetaData signCloudMetaData) throws Exception {

        SignCloudReq signCloudReq = new SignCloudReq();
        signCloudReq.setRelyingParty(Constants.relyingParty);
        signCloudReq.setAgreementUUID(agreementUUID);
        signCloudReq.setAuthorizeMethod(Constants.AUTHORISATION_METHOD_SMS);
        signCloudReq.setMessagingMode(Constants.ASYNCHRONOUS_CLIENTSERVER);

        signCloudReq.setNotificationTemplate(notificationTemplate);
        signCloudReq.setNotificationSubject(notificationSubject);

        signCloudReq.setSignCloudMetaData(signCloudMetaData);
        signCloudReq.setSigningFileData(sigingFileData);
        signCloudReq.setSigningFileName(signingFileName);
        signCloudReq.setMimeType(mimeType);

        CredentialData credentialData = createCredentialData();
        signCloudReq.setCredentialData(credentialData);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = sendPost(Constants.FUNCTION_PREPAREFILEFORSIGNCLOUD, objectMapper.writeValueAsString(signCloudReq));
        SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

        logger.info("[prepareFileForSignCloud] signCloudResp : {}", JsonMapper.writeValueAsString(signCloudResp));
        return jsonResponse;
    }

    @Override
    public String prepareMultipleFilesForSignCloud(PrepareMultipleFileRequest request) throws Exception {

        SignCloudReq signCloudReq = new SignCloudReq();
        signCloudReq.setRelyingParty(Constants.relyingParty);
        signCloudReq.setAgreementUUID(request.getAgreementUUID());
        signCloudReq.setAuthorizeMethod(Constants.AUTHORISATION_METHOD_SMS);
        signCloudReq.setMessagingMode(Constants.ASYNCHRONOUS_CLIENTSERVER);

        signCloudReq.setNotificationTemplate(request.getNotificationTemplate());
        signCloudReq.setNotificationSubject(request.getNotificationSubject());

        signCloudReq.setMultipleSigningFileData(request.getListOfSigningFileData());

        CredentialData credentialData = createCredentialData();
        signCloudReq.setCredentialData(credentialData);

        ObjectMapper objectMapper = new ObjectMapper();
        
        String jsonResponse = sendPost(Constants.FUNCTION_PREPAREFILEFORSIGNCLOUD, objectMapper.writeValueAsString(signCloudReq));
        SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

        logger.info("[prepareMultipleFilesForSignCloud] signCloudResp : {}", JsonMapper.writeValueAsString(signCloudResp));
        return signCloudResp.getBillCode();
    }

	@Override
	public SignCloudResp authorizeCounterSigningForSignCloud(AuthCounterSignRequest request) throws Exception {

		SignCloudReq signCloudReq = new SignCloudReq();
		signCloudReq.setRelyingParty(Constants.relyingParty);
		signCloudReq.setAgreementUUID(request.getAgreementUUID());
		signCloudReq.setMessagingMode(Constants.SYNCHRONOUS);
		signCloudReq.setBillCode(request.getBillCode());
		signCloudReq.setAuthorizeCode(request.getAuthorizeCode());

		CredentialData credentialData = createCredentialData();
		signCloudReq.setCredentialData(credentialData);

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = sendPost(Constants.FUNCTION_AUTHORIZECOUNTERSIGNINGFORSIGNCLOUD,
				objectMapper.writeValueAsString(signCloudReq));
		SignCloudResp signCloudResp = objectMapper.readValue(jsonResponse, SignCloudResp.class);

		logger.info("[authorizeCounterSigningForSignCloud] signCloudResp : {}",
				JsonMapper.writeValueAsString(signCloudResp));

		if (signCloudResp.getResponseCode() == 0) {
			// for single file
			if (signCloudResp.getSignedFileData() != null) {
				String file = FILE_DIRECTORY + signCloudResp.getSignedFileName() + ".signed."
						+ signCloudResp.getSignedFileName();
				logger.info("[authorizeCounterSigningForSignCloud] single file : {}, mime type : {}", file,
						signCloudReq.getMimeType());
				IOUtils.write(signCloudResp.getSignedFileData(), new FileOutputStream(file));
			} else {
				logger.info("[authorizeCounterSigningForSignCloud] signCloudResp : {}",
						JsonMapper.writeValueAsString(signCloudResp));
			}

			// for multiple files
			if (signCloudResp.getMultipleSignedFileData() != null) {
				if (!signCloudResp.getMultipleSignedFileData().isEmpty()) {
					for (int i = 0; i < signCloudResp.getMultipleSignedFileData().size(); i++) {
						MultipleSignedFileData multipleSignedFileData = signCloudResp.getMultipleSignedFileData()
								.get(i);
						if (multipleSignedFileData.getSignedFileData() != null) {
							String file = FILE_DIRECTORY + multipleSignedFileData.getSignedFileName() + "." + i
									+ ".signed.pdf";
							logger.info("[authorizeCounterSigningForSignCloud] multiple file : {}, mime type : {}",
									file, signCloudReq.getMimeType());
							IOUtils.write(multipleSignedFileData.getSignedFileData(), new FileOutputStream(file));
						} else {
							logger.info("[authorizeCounterSigningForSignCloud] multipleSignedFileData : {}",
									JsonMapper.writeValueAsString(multipleSignedFileData));
						}
					}
				}
			}
		}
		return signCloudResp;
	}
}
