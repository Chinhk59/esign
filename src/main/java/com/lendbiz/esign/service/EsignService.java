package com.lendbiz.esign.service;

import com.lendbiz.esign.dto.request.AuthCounterSignRequest;
import com.lendbiz.esign.dto.request.PrepareCertificateRequest;
import com.lendbiz.esign.dto.request.PrepareMultipleFileRequest;
import com.lendbiz.esign.dto.request.ReGenerateAuthCodeRequest;
import com.lendbiz.esign.model.SignCloudResp;

public interface EsignService {

	public String prepareCertificateForSignCloud(PrepareCertificateRequest request) throws Exception;
	
	public String prepareMultipleFilesForSignCloud(PrepareMultipleFileRequest request) throws Exception;
	
	public SignCloudResp authorizeCounterSigningForSignCloud(AuthCounterSignRequest request) throws Exception;
	
	public String regenerateAuthorizationCodeForSignCloud(ReGenerateAuthCodeRequest request) throws Exception;
}
