package com.lendbiz.esign.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lendbiz.esign.entity.CfMast;


@Repository("cfMastRepository")
public interface CfMastRepository extends JpaRepository<CfMast, String> {
	
}
